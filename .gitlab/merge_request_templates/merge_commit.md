## Why this Merge-Request ?

*

## Please explain, what changes you introduced:

*

## What Part(s) of LabPyUi is affected ?  (e.g. labpyui, docs, examples )

*

## Are there backwards-compatible changes ?

*

## Are there expected incompatible changes / side effects

*

## Checklist
+ [ ] linked to related GitLab issues ?
+ [ ] added relevant changes to the `CHANGELOG.md` in the `[vNext]` section?
