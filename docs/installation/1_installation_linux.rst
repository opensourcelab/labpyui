LabPyUi installation on Linux
__________________________________

Installation of sila 2 can be as simple as writing:

.. code:: bash

    pip3 install labpyui
    
But it is highly recommended to do this in a virtual environment.
